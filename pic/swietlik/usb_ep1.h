#ifndef _USB_EP1_H_
#define  _USB_EP1_H_

extern void USB_EP1_init(void);
extern void USB_EP1_transmission(void);

extern void USB_EP2_send(char *scr);
extern void USB_EP2_init(void);
extern void USB_EP2_transmission(void);


#endif // _USB_EP1_H_

