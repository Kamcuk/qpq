#ifdef _DS18X20_CONFIG_H_
#DEFINE _DS18X20_CONFIG_H_ 1

// DS18x20 EERPROM support disabled(0) or enabled(1) :
#define DS18X20_EEPROMSUPPORT     0
// decicelsius functions disabled(0) or enabled(1):
#define DS18X20_DECICELSIUS       1
// max. resolution functions disabled(0) or enabled(1):
#define DS18X20_MAX_RESOLUTION    0
// extended output via UART disabled(0) or enabled(1) :
#define DS18X20_VERBOSE           0

#endif
