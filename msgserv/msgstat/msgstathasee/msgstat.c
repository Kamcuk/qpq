/*
 * msgstat.c
 * 
 * Copyright 2012 Kamil Cukrowski <kamil@dyzio.pl>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#define MSGSTAT_VERSION "2.0.1"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <fcntl.h>
#include <syslog.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdarg.h>
#include <msgserv.h>
#include <common.h>
#define MY_NAME "msgstat"
#define PID_FILE "/tmp/msgstat.pid"
#define CPUNUM 1

in_addr_t server_ip;
int debug = 0;
int PID;
int sockfd;
int refresh_time = 3;

struct cpu_use_type {
  int use, total;
};

struct net_use_type {
  unsigned long long rx;
  unsigned long long tx;
};

void safe_exit() 
{
	msglcd_disconnect(sockfd);
	exit(0);
}

#define fatal(format, ...)\
do {\
	PERROR(format, ##__VA_ARGS__);\
	safe_exit();\
} while(0)

void sig_hup (int param) {
	PDEBUG("Signal received. Going down");
	safe_exit();
}

static int __attribute__((format (printf, 3, 4))) 
	mysnprintf (char *s, int n, const char *format, ...)
{
	/* its same as snprintf
	 * but does not write '\0' on the end of the strin
	 * and returns number of chars *written*
	 * i dont even care about checking - be carefull */
	char msg[256];
	short int i;
	
	va_list va;
	va_start (va, format);
	vsnprintf (msg, sizeof(msg), format, va);
	msg[sizeof(msg) - 1] = 0;
	va_end (va);
	
	for (i=0; i < n && i < 256 && i < strlen(msg); ++i) {
		s[i] = msg[i];
	}
	return(i);
}

void parse_cmd(int argc, char *argv[])
{
	int c=0;
	opterr=0;
	while ( (c = getopt(argc, argv, "hdr:i:")) != -1 )
	switch (c) {
	case 'h':
		printf("MsgLCD: messaging system to LCD geteway.\n"
		"\t-h\tdisplay this help.\n"
		"\t-d\tdebug mode on (do not fork into the background);\n"
		"\t-r\tset refresh time in secends.\n"
		"\t-i<IP address>\t server ip address.\n");
		exit(0);
	case 'd':
		debug=1;
		break;
	case 'r':
		refresh_time=atoi(optarg);
		break;
	case 'i':
		server_ip=inet_addr(optarg);
		break;
	}
}

float get_temp (int number) 
{
	FILE *file_fd;
	char buff[8], name[44];
	/*
	 * hwmon0 -> temperatura procesora
	 */
	switch(number) {
		case 1:
			snprintf(name, 43, "/sys/class/hwmon/hwmon0/device/temp2_input");
			break;
		default:
			PDEBUG("get_tempo wrong numba");
			goto GET_TEMP_ERROR;
	}
	file_fd=fopen(name,"r");
	if ( !file_fd ) {		
		PDEBUG("error opening file: %d %s.", errno, strerror(errno));
		goto GET_TEMP_ERROR;
	}
	fgets(buff, 8, file_fd);
	fclose(file_fd);
	return((float)atoi(buff)/(float)1000);
	
GET_TEMP_ERROR:
	return 0;
}

static void get_cpu_usage(float *cpuuse) 
{
	static struct cpu_use_type cpuuse_old[CPUNUM] = { [0 ... CPUNUM-1] = {0,0} };
	int i=0;
	char buff[256];
	FILE *stat_fd;
	
	struct {
		long int user, nice, sys, idle, iowait, hirq, sirq, use, hgw;
	} cpu_usage;
	
	if ( (stat_fd = fopen("/proc/stat","r")) == NULL )
		return;
	for (i=0; ( i<CPUNUM ) && ( fgets(buff,256,stat_fd) != NULL ) &&
		( sscanf(buff+5,"%ld %ld %ld %ld %ld %ld %ld %ld\n",
			&cpu_usage.user,
			&cpu_usage.nice,
			&cpu_usage.sys,
			&cpu_usage.idle,
			&cpu_usage.iowait,
			&cpu_usage.hirq,
			&cpu_usage.sirq,
			&cpu_usage.hgw) == 8 ) ; i++ ) {
		const int use = cpu_usage.user+cpu_usage.nice+cpu_usage.sys+cpu_usage.hirq+cpu_usage.sirq;
		const int total = cpu_usage.idle+cpu_usage.iowait+cpu_usage.user+cpu_usage.nice+cpu_usage.sys+cpu_usage.hirq+cpu_usage.sirq;
		
		if ( total == cpuuse_old[i].total ) // avoid dividing throu zero
			cpuuse[i]=0;
		else
			cpuuse[i]=(float)((use-cpuuse_old[i].use)*100) / (float)( total-cpuuse_old[i].total);
		cpuuse_old[i].use=use;
		cpuuse_old[i].total=total;
	
	}
	fclose(stat_fd);
}

void get_net_use( struct net_use_type *netuse, char *dev) 
{
	char buff[1024];
	FILE *filefd;
	unsigned long int tmp;
	if ( (filefd=fopen("/proc/net/dev","r")) == NULL ) {
		(*netuse).rx=999999;
		(*netuse).tx=999999;
		return;
	}
	while ( (fgets(buff,1024,filefd) != NULL ) && ( strncmp(buff, dev, 7) != 0 ) );
	if( feof(filefd) ) {
		(*netuse).rx=0;
		(*netuse).tx=0;
		return;
	}
	sscanf(buff+7,
		"%llu %lu %lu %lu %lu %lu %lu %lu "
		"%llu %lu %lu %lu %lu %lu %lu %lu",
		&(*netuse).rx, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp,
		&(*netuse).tx, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp);
	fclose(filefd);
}

static void readable_net_size(char *buff, unsigned long long n)
{
	n/=refresh_time;                                                                                                      
	if ( n < 1024 ) // 1 - 1024                                                                                           
		mysnprintf(buff, 5, "%4llub", n);
	else if ( n < 10240 ) // 1K - 10K                                                                                     
		mysnprintf(buff, 5, "%1.2fK", (float)n / (float)1024);
	else if ( n < 102400 ) // 10K - 100K                                                                                  
		mysnprintf(buff, 5, "%2.1fK", (float)n / (float)1024);
	else if ( n < 1024000 ) // 100K - 1024K                                                                               
		mysnprintf(buff, 5, "%4lluK", n / 1024);                                                                        
	else if ( n < 10485760 ) // 1M - 10M                                                                                  
		mysnprintf(buff, 5, " %1.1fM", (float)n / (float)1048576);
	else if ( n < 104857600 ) // 10M - 100M , 1048576 = 1024^2
		mysnprintf(buff, 5, "%2.1fM", (float)n / (float)1048576);
	else if ( n < 1073741824 ) // 100M - 1024M , 1073741824 = 1024^3
		mysnprintf(buff, 5, "%4lluM", n / 1048576 );
	else // > 1Gb
		mysnprintf(buff, 5, "%4lluGb", n / 1073741824 );
}

static void readable_cpu_usage(char *where, float n) 
{
	if ( n < 10 )
		mysnprintf(where, 3, "%1.1f", n);
	else 
		mysnprintf(where, 3, "%3.0f", n);
}

static int meminfo(void)
{
	FILE *fp;
	char buf[256];
	unsigned long kb_main_total, kb_main_free, kb_main_cached, kb_main_buffers;
	fp = fopen("/proc/meminfo", "r");
	if (!fp) fatal("huj");
	fgets(buf, 256, fp);
	sscanf(strchr(buf, ':'), ": %lu", &kb_main_total);
	fgets(buf, 256, fp);
	sscanf(strchr(buf, ':'), ": %lu", &kb_main_free);
	fgets(buf, 256, fp);
	sscanf(strchr(buf, ':'), ": %lu", &kb_main_buffers);
	fgets(buf, 256, fp);
	sscanf(strchr(buf, ':'), ": %lu", &kb_main_cached);
	fclose(fp);

	return((kb_main_total - kb_main_free - kb_main_cached - kb_main_buffers)/1024);
}

static void handle_msglcd_handle_server(const int lcd_sock) 
{
	int i;
	switch ( (i = msglcd_handle_server(lcd_sock)) ) {
	case -5:
	case -1:
		fatal("Strange error occured %d ", i);
	case -4:
		fatal("Server closed the connection. %d ", i);
	case -6:
		fatal("server sended error messege %d", i);
	case -2:
		fatal("Server closed the connection without sending exit mesg. %d ", i);
	case -3:
		fatal("my mesg has somth wrong %d ", i);
	}
}

static void handle_msg_updat(const int sockfd, const char nr, const char pri, const char *mesg, const int len) 
{
	int i;
	i = msglcd_updat(sockfd, nr, pri, mesg, len);
	if ( i == 0 ) return;
	fatal("could not update mesg. msglcd_updat returned: %d", i);
}

int main(int argc, char **argv) 
{
	struct net_use_type net_old = {0,0};
	
	char buff[4][41] = {
		[0] = "                               up   :   ",
		[1] = "CPU:    % Ram used:    MB Temp:    C    ",
		[2] = "leonidas:   in:       out:              ",
	};
	
	parse_cmd(argc, argv);

	if (!server_ip) {
		fprintf(stderr, "Please give msgsrv ip address.\n");
		safe_exit();
	}
	
	signal(SIGHUP,sig_hup);
	signal(SIGQUIT,sig_hup);
	signal(SIGTERM,sig_hup);
	signal(SIGINT,sig_hup);
	
	sockfd = msglcd_connect(server_ip);
	if (sockfd < 0) fatal("Could not connect to lcd server %d \n", sockfd);

	if ( !debug ) { 
		openlog(MY_NAME,LOG_PID,LOG_DAEMON);
		syslog(LOG_INFO,"Startup  succeeded.");
	}
	

	PDEBUG("Connected with %s.\n", inet_ntoa(*(struct in_addr *)&server_ip));
  
	for (;;) {
		int i;
		
		
		/* first line, write time */
		{ /* date | centereed int the first 30 chars */
		const time_t CurentTime = time(NULL);
		int len;
		len = strftime(buff[0], 31, "%H:%M %A %d/%m/%y", localtime(&CurentTime));
		if ( !len ) 
			fatal("strftime");
		if ( len%2 ) {
			len/=2;
			for(i = 30; i >= 15+len+1; --i) /* clean before date */
				buff[0][i] = ' ';
			for(; i >= 15-len; --i) /* move the date from the beggining to the middle */
				buff[0][i] = buff[0][i - 15 + len];
			for(;i>=0; --i) /* clean the beggining */
				buff[0][i] = ' ';
		} else {
			len/=2;
			for (i = 30; i >= 15+len; --i) /* clean before date */
				buff[0][i] = ' ';
			for (; i >= 15-len ; --i) /* move the date from the beggining to the middle */
				buff[0][i] = buff[0][i - 15 + len];
			for (;i>=0; --i) /* clean the beggining */
				buff[0][i] = ' ';
		}
		}
		
		{ /* uptime */
			/* get sysinfo info */
			struct sysinfo sys_info;
			if (sysinfo(&sys_info) != 0) fatal("sysinfo");
			int hours = sys_info.uptime / 3600;
			mysnprintf(buff[0]+33, 3, "%3d", hours);
			mysnprintf(buff[0]+37, 2, "%2ld", (sys_info.uptime / 60) - (hours * 60));
		}
		
		/* second line */
		{
			float cpuuse[CPUNUM];
			/* get cpu usage */
			get_cpu_usage(cpuuse);
			for (i = 0; i < CPUNUM; i++) {
				readable_cpu_usage(buff[1]+5*i+5, cpuuse[i]);
			}
		}
		/* write total used ram */
		mysnprintf(buff[1]+20, 4, "%4d", meminfo());
		
		/* third line, temperatures */
		mysnprintf(buff[1]+33, 4, "%2.0f",
			get_temp(1));
		
		/* forth line, write bandwitch speed */
		{
			struct net_use_type net_new;
			/* GET bandwitch rx and tx speed */
			get_net_use(&net_new,"  eth0:");
			readable_net_size(buff[2]+16, net_new.rx-net_old.rx);
			readable_net_size(buff[2]+27, net_new.tx-net_old.tx);
			net_old=net_new;
		}
		
		/*write  when debug, buffers on the screen*/
		if ( debug ) {
			for (i = 0; i < 40; i++)
				printf("%c", buff[0][i]);
			printf("\n");
			for (i = 0; i < 40; i++)
				printf("%c", buff[1][i]);
			printf("\n");
			for (i = 0; i < 40; i++)
				printf("%c", buff[2][i]);
			printf("\n");
		}
		
		/* send the message */
		handle_msg_updat(sockfd, 0, MSG_INFO, buff[0], 40);
		handle_msg_updat(sockfd, 1, MSG_INFO, buff[1], 40);
		handle_msg_updat(sockfd, 2, MSG_INFO, buff[2], 40);
		/* handle server messages */
		handle_msglcd_handle_server(sockfd);
		
		sleep(refresh_time);
	}
	return(0);
}

