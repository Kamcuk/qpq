/*
 * lcd.h
 * 
 * Copyright 2012 Kamil Cukrowski <kamil@dyzio.pl>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef _LCD_H_
#define _LCD_H_

#define LCD_FILE "/dev/uhc1"
#define LCD_COL 40
#define LCD_ROW 16
#define LCD_buff_size 256

void LCD_set_file();
int LCD_rst();
int LCD_init();
int LCD_print(char *text, int size);
int LCD_print_xy(char col, char row, char *text, int size);
int LCD_clr_line(int line);
int LCD_fsync();
void LCD_close();
int LCD_ok(void);
#endif
