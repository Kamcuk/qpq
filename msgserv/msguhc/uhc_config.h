#ifndef _UHC_CONFIG_H_
#define _UHC_CONFIG_H_

/* Define these values to match your devices */
#define UHCMOD_USB_VENDOR_ID 0xffff
#define UHCMOD_USB_PRODUCT_ID 0x0003

#define DISP_COLS 40  /* number of columns every hd44780 has */
#define MAX_DISP_ROWS 16

#define ROWS_PER_CTRL 2 /* number of rows every hd44780 has */
//#define DISP_ROWS ((ROWS_PER_CTRL * NUMBER_OF_LCDS))  /* number of rows  HELL OF A BAD THING! */
//#define ONE_CTRL_DISP_SIZE ((DISP_COLS*ROWS_PER_CTRL)) /* number of cells in one controller */
//#define DISP_SIZE ((DISP_COLS*DISP_ROWS)) /* size of a whole display */

#define RS_DATA     (0x02)
#define RS_INST     (0x00)
#define RW_READ     (0x01)
#define RW_WRITE    (0x00)

#define CAKE_RW_BIT       0b00000001
#define CAKE_RS_BIT       0b00000010
#define CAKE_CTRL_NR_BITS 0b00011100
#define CAKE_FILLING_BITS 0b11100000
#define CAKE_FLAG_BITS    ((CAKE_RS_BIT | CAKE_RW_BIT))

// packet modes 
#define CAKE_FILLING_RAW 		0<<5
#define CAKE_FILLING_ALL_RAW 		1<<5
#define CAKE_FILLING_NUM_READ 		2<<5
#define CAKE_FILLING_SET_READ 		3<<5
#define CAKE_FILLING_GET_CONFIG 	4<<5
#define CAKE_FILLING_GET_INFO	 	5<<5


#endif /* _UHCMOD_CONFIG_H_ */

