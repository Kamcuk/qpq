/*
 * lcd.h
 * 
 * Copyright 2014 Kamil Cukrowski <kamil@dyzio.pl>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef _LCD_H_
#define _LCD_H_

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>


#include <libusb-1.0/libusb.h>

#include "uhc_config.h"

struct libusb_device_handle *usb_connect(libusb_context *ctx, int vendor, int product);
struct libusb_device_handle *usb_disconnect(libusb_context *ctx, struct libusb_device_handle *handle);


int LCD_rst();
int LCD_print(const char *text, int size);
int LCD_print_line(const unsigned char row, const char const *text);
int LCD_clr_line(const unsigned int row);
int LCD_ok();
void LCD_close();
int LCD_init();
#endif
