#/bin/bash

TIME=$1
shift

if test -z "$1" ; then
        inter="eth0 eth1"
else
        inter=$@
fi

for i in $inter; do 
	eval "or_${i}=0"
	eval "ot_${i}=0"
	eval "nr_${1}=0"
	eval "nt_${1}=0"
done

while sleep $TIME; do 
	a=$(cat /proc/net/dev);
	msg=""
	for i in $inter; do	
		eval "b=\$(echo \"\$a\"|grep ${i})"
#		eval "nr_${i}=\$(echo \"\$b\"|awk '{print \$1}'|sed 's/${i}://')"
		eval "nr_${i}=\$(echo \"\$b\"|sed 's/.*:[ ]\([0-9]*\) .*/\1/')"
		eval "nt_${i}=\$(echo \"\$b\"|awk '{print \$9}')"
		eval "msg=\"\$msg\"\"\$[\$nr_${i}-\$or_${i}]\"\"\\n\""
		eval "msg=\"\$msg\"\"\$[\$nt_${i}-\$ot_${i}]\"\"\\n\""
		eval "or_${i}=\$nr_${i}"
		eval "ot_${i}=\$nt_${i}"
	done
	echo -ne "$msg"
done
