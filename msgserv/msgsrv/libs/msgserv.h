/*
 * msgsrv.h
 * 
 * Copyright 2012 Kamil Cukrowski <kamil@dyzio.pl> and Grześ Sójka <grzes@sojka.co>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef _MSGSERV_H_
#define _MSGSERV_H_

#include  <netinet/in.h>

/* priority definitions */
#define MSG_EMERG 	0x00 /* system is unusable               */
#define MSG_ALERT 	0x10 /* action must be taken immediately */
#define MSG_CRIT 	0x20 /* critical conditions              */
#define MSG_ERR 	0x30 /* error conditions                 */
#define MSG_WARNING 	0x40 /* warning conditions               */
#define MSG_NOTICE 	0x50 /* normal but significant condition */
#define MSG_INFO 	0x60 /* informational                    */
#define MSG_DEBUG 	0x70 /* debug-level messages             */
	/* bigger then 0x07 - dont give a shit */
/* modes definitions */
#define MSG_UP 'u'                    // update message
#define MSG_DEL 'd'                   // delete !ALL! messages sent by a given client
#define MSG_LCD 'l'                     /* send immidatly to lcds           */
/* connects to msgsrv definitions */
#define MSG_CONNECT_CLI 	'C'
#define MSG_CONNECT_LCD 	'L'
#define MSG_CONNECT_UNSPECIFIED	'U'
/* messeges, they all need to be 3 chars and start with # */
#define MSGLCD_MESG_PING 	"#PI"
#define MSGLCD_MESG_OK  	"#OK"
#define MSGLCD_MESG_ERROR 	"#ER"
#define MSGLCD_MESG_EXIT 	"#EX"
#define MSGLCD_MESG_TOMA 	"#TO"
#define MSGLCD_MESG_LENGTH	7 /* all MSGLCD_ messages in this length */
/* more to come */

#define MSGLCD_PORT 2544          /* the port users will be connecting to */
#define MSG_LEN 40

struct message_struct {
	union {
		int32_t guest;			// client id
		int32_t client;
	};
	struct in_addr addr;		// client address
	unsigned char nr;		// message number assigned by client
	unsigned char pri;		// priority
	time_t time;			// last time transmitted message
	
	/* THIS NEEDS TO BE DECLARED LAST (see deleting one mesg) */
	unsigned char mesg[MSG_LEN+1];	// the message
};



#define msglcd_up(...) msglcd_update( __VA_ARGS__ )
#define msglcd_del(sockfd, nr) msglcd_updat(sockfd, nr, 0, NULL, 0)
#define msglcd_connect(ip) msglcd_connect_client(ip)
#define msglcd_connect_client(ip) msglcd_socket_connect(ip, MSG_CONNECT_CLI)
#define msglcd_connect_display(ip) msglcd_socket_connect(ip, MSG_CONNECT_LCD)
#define msglcd_disconnect(sockfd) msglcd_socket_disconnect(sockfd)


/* functions definitions */

extern int msglcd_recv(const int sockfd, char *buff, const int len);

extern int msglcd_send(const int sockfd, const char *buff, const int len);

extern int msglcd_updat(const int sockfd, const char nr, const char pri, const char *mesg, const int len);
extern int msglcd_send_to_lcd(const int sockfd, const char *mesg, const int len) ;
extern void msglcd_del_allmesg(const int sockfd);

/* 
 * msglcd_handle_server
 * use it every time having problem with msgsrv (like updating messege or smth ) 
 * RETURN 0 on success, 
 * return negativ on error, sometimes errno should be set 
 * return -4 means server closed conection 
 * */
extern int msglcd_handle_server(const int fd);

/* 
 * msglcd_socket_connect
 * connects and disconnects with msgsrv on given server_ip
 * server_ip - in_addr_t structure filled with server ip address running msgsrv service
 * sign - one of the MSG_CONNECT_* defines
 * return sockfd as returned by accept, error if retcal < 0
 * */
extern int msglcd_socket_connect(const in_addr_t server_ip, const char sign);

/*
 * msglcd_socket_disconnect
 * disconnects sockfd 
 * */
extern void msglcd_socket_disconnect(const int sockfd);

#endif /* _MSGSERV_HEADER_ */ 
