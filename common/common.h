#ifndef _COMMON_H_
#define _COMMON_H_

#include <syslog.h>
#include <time.h>

/* substitution 
 * 
#define PDEBUG(str, ...) PDEBUGL(1, str, ##__VA_ARGS__);
#define PDEBUGL(x, str, ...) do{ if ( debug < -x ) INTERNAL_LOGIT(str, ##__VA_ARGS__); }while(0)
#define LOGIT(str, ...) INTERNAL_LOGIT(str, ##__VA_ARGS__)
#define PINFO(str, ...) INTERNAL_LOGIT(str, ##__VA_ARGS__)
#define PERROR(str, ...) do { printf("msguhc: (%s:%d): ", __FILE__, __LINE__); printf(str, ##__VA_ARGS__); printf("error %d:%s \n", errno, strerror(errno)); }while(0)
#define INTERNAL_LOGIT(str, ...) do { printf("msguhc: (%s:%d): ", __FILE__, __LINE__); printf(str, ##__VA_ARGS__); }while(0)
* */

/* you want to turn debbuging off?
 * you only need to define COMMON_DEBUG_OFF
 * and then all this is just a placeholder! */
#ifdef COMMON_DEBUG_OFF
 	#define PDEBUG(...)
	#define PDEBUGL(x, ...)
	
	#define PDEBUG2(...) do{ char __PDEBUG2 __attribute__ ((deprecated)); __PDEBUG2; }while(0)
#else
	/* debug logging */
	#define PDEBUG(...) internal_logit(1,  LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
	/* long debug, you can set the debug level of the message, which will be checked >= debug */
	#define PDEBUGL(x, ...) internal_logit(x, LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
	
	#define PDEBUG2(...) do{ char __PDEBUG2 __attribute__ ((deprecated)); __PDEBUG2; internal_logit(2, LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); } while(0)

#endif


/* put something into the log */
#define LOGIT(...) internal_logit(-1, LOG_INFO, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#define PINFO(...) internal_logit(-1, LOG_INFO, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
/* log errno number, returned string by strerror(errno)  and your message */
#define PERR PERROR
#define PERROR(...) internal_logit(-1, LOG_ERR, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)


/* initialie and close log and all common functions
 * level - debug level to initiate,
 * my_name -  */
extern void common_open(const int level, const char *my_name);
extern void common_close(void);

/* Put something into the log
 * level - requested debug level 
 * it checks -> if ( (debug == 0 && type == LOG_DEBUG) || debug < level) return;
 * then if debug == 0 -> put smth into syslog with type
 * debug != 0 and level >= debug - put smth on the screen (now type takes no effect) 
 * when (type >= LOG_ERR) then it also logs number errno and output of strerrno(errno) */
extern void __attribute__((format(printf,6,7))) internal_logit
(const int level, const int type, const char *file, const int line, const char *function, const char *format, ...);

/* daemonize - fork a program
 * PARENT DOES exit(0); INSIDE THIS FUNCTION!!
 * RETVAL (to child): 0 - success, -1 - failure */
extern int daemonize(void);
/* daemonize_store_pid 
 * - forks a program and stores new pid in file store_pid
 * the parent saves pid, child continues to work 
 * RETVAL: 0 - succes, -1 - failure */
extern int daemonize_store_pid(const char *store_pid);

#endif /* #ifndef _COMMON_H_ */
