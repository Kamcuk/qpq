/*
 * baza.h
 * 
 * Copyright 2013 Kamil Cukrowski <kamil@dyzio.pl>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef BAZA_H
#define BAZA_H 1

#include <string>

class IBaza {
 public:
	virtual ~ IBaza() {
	}
	virtual void DodajWpis(unsigned poziom_katalogu, const std::string &protocol,
			const std::string &hostname, const std::string &path, 
			const std::string &file, unsigned long long rozmiar) {}
	virtual void flush() {}
};

IBaza *OtworzBazePlik(const std::string &hostname, std::ostream &plik_log);

#endif // BAZA_H
