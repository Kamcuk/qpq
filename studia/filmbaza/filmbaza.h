/** @file filmbaza.h */
#ifndef FILMBAZA_H
#define FILMBAZA_H

#include <iostream> /* string */
#include "list.h" /* film_s */

/**
 * @brief struktura pojedynczego filmu
 */
struct film_s {
	int id;
	
	std::string name;
	
	int rate;
	
	struct {
		bool shared;
		std::string name;
	} share;
	
    film_s *next;
};

/**
 * @brief definicja bazy danych
 */
struct filmbaza {
	std::string name;
	
    film_s *filmy;
};

struct film_s *filmbaza_find_film_id(struct filmbaza *fb, int id);

struct filmbaza *filmbaza_create(std::string name);
void filmbaza_destroy(struct filmbaza *fb);

int filmbaza_add(struct filmbaza *fb, struct film_s info);
int filmbaza_add(struct filmbaza *fb, std::string name);
void filmbaza_delete(struct filmbaza *fb, int id);
void filmbaza_delete_all_films(struct filmbaza *fb);

void filmbaza_rate(struct filmbaza *fb, int id, int rate);
void filmbaza_share(struct filmbaza *fb, int id, std::string friend_name);
void filmbaza_unshare(struct filmbaza *fb, int id);

#define FILMBAZA_SORT_ID 0
#define FILMBAZA_SORT_NAME 1
#define FILMBAZA_SORT_RATE 2
#define FILMBAZA_SORT_SHARE_SHARED 3
#define FILMBAZA_SORT_SHARE_NAME 4
void filmbaza_sort(struct filmbaza *fb, int opt, int direction);

#endif

