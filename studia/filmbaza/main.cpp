#include <QApplication>
#include "logwindow.h"
#include "mainwindow.h"
#include "login.h"
#include "cmd_interface.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setOrganizationName("Kamil Cukrowski");
        a.setApplicationName("filmoteka");

    logwindow log;
    log.show();
    a.exec();

    if ( logged() ) {
        MainWindow w;

        w.show();
        a.exec();
    }
	
	return 0;
}
