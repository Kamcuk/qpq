#-------------------------------------------------
#
# Project created by QtCreator 2013-04-20T12:50:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = flimobaza
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    list.cpp \
    eximport.cpp \
    filmbaza.cpp \
    cmd_interface.cpp \
    login.cpp \
    scan.cpp \
    logwindow.cpp

HEADERS  += mainwindow.h \
    login.h \
    list.h \
    filmbaza.h \
    eximport.h \
    defines.h \
    cmd_interface.h \
    scan.h \
    logwindow.h

FORMS    += mainwindow.ui \
    logwindow.ui
