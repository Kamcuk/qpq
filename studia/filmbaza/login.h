/** @file login.h */
#ifndef LOGIN_H
#define LOGIN_H

#include <iostream>
	
int uwierzytelnij(std::string user, std::string password);
int admin();
int logged();
const std::string get_user();
int login_useradd(std::string user, std::string pass, int mode);
int login_userdel(std::string user);

#endif // LOGIN_H
