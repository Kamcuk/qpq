/** @file */
#ifndef LOGWINDOW_H
#define LOGWINDOW_H

#include <QMainWindow>

namespace Ui {
class logwindow;
}

class logwindow : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit logwindow(QWidget *parent = 0);
	~logwindow();
	
private slots:
	void on_username_returnPressed();
	void on_password_returnPressed();
	
	void on_pushButton_2_clicked();
	void on_pushButton_clicked();
	
private:
	Ui::logwindow *ui;
};

#endif // LOGWINDOW_H
