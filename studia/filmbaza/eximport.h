/** @file eximport.h */
#ifndef EXIMPORT_H
#define EXIMPORT_H

#include "filmbaza.h"
#include <iostream>

int filmbaza_export(struct filmbaza *fb, std::string file_name);
int filmbaza_import(struct filmbaza *fb, std::string file_name);

#endif // EXIMPORT_H
