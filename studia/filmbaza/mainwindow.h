/** @file */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <QModelIndex>

#include "filmbaza.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	
private slots:
	
	/* ikonki */
	void on_buttonStworz_baze_clicked();	
	void on_buttonUsu_baze_clicked();
	void on_buttonDodaj_film_clicked();
	void on_buttonUsu_film_clicked();
	
	/* tablica edit */
	void on_tabka_doubleClicked(const QModelIndex &index);
	
	/* menu filmbaza */
	void on_actionNowa_filmbaza_triggered();
	void on_actionUsun_filmbaze_triggered();
	void on_actionImportuj_triggered();
	void on_actionEksportuj_triggered();
	void on_actionZako_cz_triggered();
	
	/* menu film */
	void on_actionDodaj_film_triggered();
	void on_actionUsu_film_triggered();
	void on_actionSortuj_triggered();
	
	
	/* menu o nas */
	void on_actionO_filmbazie_triggered();
	
	/* menu dodatkowe */
	void on_actionPrzeloguj_sie_triggered();
	void on_actionSkanuj_katalog_triggered();
	
	/* menu admin */
	void on_actionDodaj_u_ytkownika_triggered();
	void on_actionUsu_u_ytkownika_triggered();

private:
	Ui::MainWindow *ui;
	filmbaza *fb;
	
	void refresh();
	void print(std::string msg);
	
	/* to mogłybybyć sloty */
	void baza_new();
	void baza_del();
	void import_base();
	void export_base();
	void film_new();
	void film_del();
	
	void dirscan();
};

#endif // MAINWINDOW_H
