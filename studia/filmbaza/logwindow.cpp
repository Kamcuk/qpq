/** @file */
#include "logwindow.h"
#include "ui_logwindow.h"

#include <QMessageBox>
#include <iostream>
#include "login.h"

#include "mainwindow.h"

logwindow::logwindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::logwindow)
{
	ui->setupUi(this);
	
	this->setWindowTitle("Zaloguj...");
	
	ui->username->setFocus(Qt::OtherFocusReason);
	ui->password->setEchoMode( QLineEdit::Password );
}

logwindow::~logwindow()
{
	delete ui;
}

void logwindow::on_pushButton_clicked()
{
	const std::string user = ui->username->text().toStdString();
	const std::string pass = ui->password->text().toStdString();
	int ret;
	
	ret = uwierzytelnij(user, pass);
	
	if ( ret < 0 ) {
		QMessageBox::information(this, "Błąd logowanie", "Nieprawidłowa nazwa użytkownika lub hasło");
		ui->username->setFocus(Qt::OtherFocusReason);
		ui->password->clear();
	} else if ( ret == 1 || ret == 0 ) {
		 /* we dissapear */
		this->close();
		QApplication::quit();
	}
	
}


void logwindow::on_pushButton_2_clicked()
{
	exit(0);
}

void logwindow::on_username_returnPressed()
{
	ui->password->setFocus(Qt::OtherFocusReason);
}

void logwindow::on_password_returnPressed()
{
	this->on_pushButton_clicked();
}
