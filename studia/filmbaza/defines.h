/** @file defines.h */
#ifndef DEFINES_H
#define DEFINES_H

/*
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#define PERROR(arg, ...) fprintf(stderr, "file:%s line:%d func:%s err:%d  strerr:%s msg:" arg "\n", __FILE__, __LINE__, __FUNCTION__, errno, strerror(errno), ## __VA_ARGS__ )
*/
#define PERROR(arg, ...)

#endif // DEFINES_H
