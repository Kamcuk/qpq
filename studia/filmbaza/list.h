/** @file list.h */
#ifndef LIST_H
#define LIST_H

#include <stddef.h> /* ofsetof() */
#include <filmbaza.h>


#define list_for_each(pos, head)  \
    for (pos = *head; pos; pos = pos->next)

#define list_for_each_safe(pos, n, head) \
    for (pos = *head, n = pos ? pos->next : NULL; pos; \
        pos = n, n = pos ? pos->next : NULL)

void list_add(struct film_s *nnew, struct film_s **head);
void list_del(struct film_s *entry, struct film_s **head);
void list_sort(void *priv, struct film_s **head,
		int (*cmp)(void *priv, struct film_s *a, struct film_s *b));


#endif
