#!/bin/bash

set -x
BR_FILE=/sys/class/backlight/intel_backlight/brightness
BR=$(cat /sys/class/backlight/intel_backlight/brightness)
BRMAX=$(cat /sys/class/backlight/intel_backlight/max_brightness)
CHUNK=$[$BRMAX/20]
BR=$[$BR/$CHUNK*$CHUNK]
case "$1" in
"+") 
	[ $BR -lt $BRMAX ] && echo $[$BR+$CHUNK] > $BR_FILE
;;
"-")
	[ $BR -gt 0 ] && echo $[$BR-$CHUNK] > $BR_FILE
;;
"get")
	echo $BR
;;
*) 
	echo dupa dup adupa 
esac


