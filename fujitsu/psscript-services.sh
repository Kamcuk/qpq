#!/bin/bash 

#/*
#* 
#* Copyright 2013 Kamil Cukrowski <kamil@dyzio.pl>
#* 
# * ----------------------------------------------------------------------------
#* "THE BEER-WARE LICENSE" (Revision 42):
#* Kamil Cukrowski <kamil@dyzio.pl> wrote this file. As long as you retain this notice you
#* can do whatever you want with this stuff. If we meet some day, and you think
#* this stuff is worth it, you can buy me a beer in return.
#* ----------------------------------------------------------------------------
#*/

# importtant  - close the  output  stream  when running in background !!!!
# declaration of fucntion parameters - start, stop, check.
# function start - starts the function thing
# function stop - stops the function thing
# function check - echo-es 1 when running, 0 when not.
#
#every function must add to services list

services="$services ethernet "
function ethernet {
case $1 in
"start")
	modprobe tg3
	usleep 500000
	ifconfig eth0 up
	dhcpcd -q -b eth0 >/dev/null &
;; "stop")
	dhcpcd -x eth0
	ifconfig eth0 down
	rmmod tg3
	rmmod ptp
	rmmod pps_core
	rmmod libphy
;; "check" | *)
	check_module tg3
;; esac
}

services=" $services bluetooth "
function bluetooth {
case $1 in
"start")
	modprobe bluetooth
;; "stop")
	rmmod bluetooth
;; *)
	check_module bluetooth
;; esac
}

services=" $services sound "
function sound {
case $1 in
"start")
	modprobe snd-intel8x0
;; "stop")
	for i in snd-intel8x0 snd_ac97_codec ac97_bus snd_pcm snd_page_alloc snd_timer snd soundcore; do
		rmmod $i
	done
;; *)
	check_module snd_intel8x0
;; esac
}

services=" $services wifi "
function wifi {
case $1 in
"start")
	modprobe ipw2200
	usleep 500000
	ifconfig wlan0 up
	wpa_supplicant -B -c/etc/wpa_supplicant/wpa_supplicant-wlan0.conf -iwlan0 -Dwext > /dev/null &
	echo $! > /tmp/.powermanagment_wpa_supplicant-wlan0
	dhcpcd -q -b wlan0 > /dev/null &
;; "stop")
	kill $(cat /tmp/.powermanagment_wpa_supplicant-wlan0)
	dhcpcd -x wlan0
	ifconfig wlan0 down
	usleep 500000
	rmmod ipw2200
;; "check" | *)
	check_module ipw2200
;; esac
}

services=" $services sshd "
function sshd {
case $1 in
"start")
	/usr/bin/sshd -D >/dev/null &
	usleep 1
;; "stop")
	kill `cat /run/sshd.pid`
;; "check" | *)
	[[ -f /run/sshd.pid ]] && echo 1 || echo 0
;; esac
}

services=" $services usb "
function usb {
case $1 in
"start")
	for i in hid usbhid psmouse usb_common usbcore uhci_hcd ehci_hcd ehci_pci ehci_hcd usbcore hid_generic; do
		modprobe $i
	done
;; "stop")
	echo this does not work that way
;; *)
	check_module uhci_hcd
;; esac
}

services=" $services pcmcia "
function pcmcia {
case $1 in
"start")
	for i in yenta-socket pcmcia pcmcia-rsrc pcmcia-core; do
		modprobe $i
	done
;; "stop")
	rmmod pcmcia yenta_socket pcmcia_rsrc pcmcia_core
;; *)
	check_module pcmcia
;; esac
}

services=" $services firewire "
function firewire {
case $1 in
"start")
	for i in firewire-ohci firewire-core crc-itu-t; do
		modprobe $i
	done
;; "stop")
	rmmod firewire-ohci firewire-core crc-itu-t
	rmmod firewire-ohci firewire-core crc-itu-t
	rmmod firewire-ohci firewire-core crc-itu-t
;; *)
	check_module firewire_core
;; esac
}

services=" $services mmc_sd "
function mmc_sd {
case $1 in
"start")
	for i in sdhci_pci sdhci mmc_core ;do
		modprobe $i
	done
;; "stop")
	rmmod sdhci_pci sdhci mmc_core
	rmmod sdhci_pci sdhci mmc_core
	rmmod sdhci_pci sdhci mmc_core
;; *)
	check_module mmc_core
;; esac
}

services=" $services cpuspeed "
function cpuspeed {
case $1 in
"start")
        echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
;; "stop")
        echo ondemand > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
;; *)
        test "$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)" == "performance" && echo 1 || echo 0
;; esac
}



services="$services dhcpcd_eth0 "
function dhcpcd_eth0 {
int=eth0
case $1 in
"start")
	dhcpcd -q -b $int >/dev/null &
;; "stop")
	dhcpcd -x $int
;; *)
	test "$(ps aux | grep 'dhcpcd -q -b '$int | wc -l )" -gt 1 && echo 1 || echo 0
;; esac
}


services="$services dhcpcd_wlan0 "
function dhcpcd_wlan0 {
int=wlan0
case $1 in
"start")
	dhcpcd -q -b $int >/dev/null &
;; "stop")
	dhcpcd -x $int
;; *)
	test "$(ps aux | grep 'dhcpcd -q -b '$int | wc -l )" -gt 1 && echo 1 || echo 0
;; esac
}
