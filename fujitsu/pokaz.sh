#!/bin/bash

user=kamil # $(ps aux | grep xfce4-session | head -n 1 | awk '{print $1}')
#FONT="-adobe-helvetica-bold-*-*-*-34-*-*-*-*-*-*-*"
FONT="-misc-fixed-medium-r-semicondensed--40-*-*-*-c-*-*-*"
OSD=" osd_cat -f $FONT -O 5 -d 1 "

file=$(mktemp)
chown $user:users $file
echo "$@" > $file
(
	sudo -u $user /bin/sh -c "export DISPLAY=:0.0; killall osd_cat; $OSD $file;"
	rm $file;
) &


