static int phc_kb_open(struct tty_struct *tty, struct file *file)
{
	PDEBUG_TTY("phc_kb_open ");
	if (phc_kb_write_open) return -EBUSY;
	++phc_kb_write_open;
	return 0;
}

static void phc_kb_close(struct tty_struct *tty, struct file *file)
{
	PDEBUG_TTY("phc_kb_close ");
	--phc_kb_write_open;
}

static int phc_kb_write(struct tty_struct *unused, const unsigned char *buf, int count)
{
	struct phc_vc_data *phc_vc;
	
	if (!current_tty) return count;
	phc_vc = current_tty->driver_data;
	if (!phc_vc) return count;
	if (!count) return 0;
	
	mutex_lock(&phc_vc->mutex);
	
	PDEBUG_TTY("phc_write ");
	if ( tty_insert_flip_string(current_tty, buf, count) ) {
		tty_flip_buffer_push(current_tty);
	}
	
	mutex_unlock(&phc_vc->mutex);
	return count;
}

static int phc_kb_write_room(struct tty_struct *tty) 
{
	return 10;
}

static int phc_ttykb_tiocmget(struct tty_struct *tty, struct file *file)
{
	PDEBUG_TTY("tiny_tiocmget\n");
	return mcr;
}

static int phc_ttykb_tiocmset(struct tty_struct *tty, struct file *file, unsigned int set, unsigned int clear)
{
	PDEBUG_TTY("tiny_tiocmset\n");
	mcr = mcr | set | ~clear;
	return mcr;
}

static const struct tty_operations phc_ttykb_ops = {
	.write = phc_kb_write,
	.open = phc_kb_open,
	.close = phc_kb_close,
	.write_room = phc_kb_write_room,
	.set_termios =  phc_tty_set_termios, /* the same */
	.tiocmget = phc_ttykb_tiocmget,
	.tiocmset = phc_ttykb_tiocmset,
};

/* ------------------- char device end --------------*/
