/* proc tree functions */
/* proc_read_lcdmod()
 * if data == NULL -  reading from /proc/lcdmod/lcdmod
 * if data == state - reading from /proc/lcdmod/lcdmod_long
 * if data == lcddev - reading from /proc/lcdmod/lcdmod_all
 * lcdmod_all should post ALL information we store ( why not? )
 * 	use it wisely
 */
static int phc_proc_read(char *buffer, char **start, off_t offset, int size, int *eof, void *data )
{
	char *temp = buffer;
	int i, j;

	/* Print module configuration */
	temp += sprintf(temp, /* I/O base */
		"I/O base:        %#8lx\n"
		, 
		phcdev.pd->port->base);
	temp += sprintf( temp, 
		" ^ NumReset:   %10lu\n"
		" ^ NumNext:    %10lu\n"
		" ^ NumEnable:  %10lu\n"
		" ^ ^ Reads:    %10lu\n"
		" ^ ^ Writes:   %10lu\n"
		" ^ ^ BfChecks: %10lu\n"
		,
		phcdev.reset_num,
		phcdev.next_num,
		phcdev.read_num+phcdev.write_num+phcdev.bfchecks_num,
		phcdev.read_num,
		phcdev.write_num,
		phcdev.bfchecks_num);
	temp += sprintf( temp, 
		" ^ Flags:        %#08lx\n"
			" ^ ^ CheckingBF:     %4s\n"
			" ^ ^ was IRQ:        %4s\n"
		,
		phcdev.flags,
		( test_bit(FLAG_NOBFCHECK, &phcdev.flags) ? "no" : "yes" ),
		( test_bit(FLAG_WAS_IRQ, &phcdev.flags) ? "yes" : "no" ));
		
	for ( j = 0; j < ctrls_num; j++) {	
		int k, l;
		temp += sprintf( temp, "Controler number %d.\n", j);
		for (k = 0; k < 8; k++) {
			temp += sprintf( temp, " ^ CGRAM[%1d]: ", k);
			for (l=0; l<8; l++) {					 
				temp += sprintf(temp, "%02x", phcdev.lcd[j].cgram[k][l]);
			}
			temp += sprintf(temp, "\n");
		}
	}
	temp += sprintf(temp,
			"DisplayRows:         %4d\n"
			"DisplayColumns:      %4d\n"
			"ControlersNumber:    %4d\n"
			,
			disp_rows, 
			disp_cols,
			ctrls_num );
	temp += sprintf(temp, 
		"RowsPerControler:    %4d\n"
		,
		row_per_ctrl);
	
	
	/* Print display state */
	temp += sprintf( temp, "+" );
	for( i = 0; i < disp_cols; i++ )
		temp += sprintf( temp, "-" );
	temp += sprintf( temp, "+" );
	temp += sprintf(temp, " ctrl_nr ");
	temp += sprintf( temp, "\n");
	
	for( i = 0; i < disp_rows ; i++ ) {
		temp += sprintf( temp, "|" );
		for( j = 0; j < disp_cols ; j++ ) {
			if ( phcdev.state[i*disp_cols+j] < 10 ) /* character from CGRAM */
				temp += sprintf( temp, "^");
			else
				temp += sprintf( temp, "%c", phcdev.state[i*disp_cols+j]);
		}
		temp += sprintf(temp, "| %d \n", (i / row_per_ctrl) );
	}

	temp += sprintf( temp, "+" );
	for( i = 0; i < disp_cols; i++ )
			temp += sprintf( temp, "-" );
	temp += sprintf( temp, "+\n" );
	
	return temp - buffer;
}
