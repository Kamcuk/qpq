#ifndef _LCDMOD_CONFIG_
#define _LCDMOD_CONFIG_

#define DFLT_BASE 0x378 /* Default I/O base address of the parallel port */
#define DFLT_DISP_ROWS_PER_CTRL  2 /* Default number of rows the display has */
#define DFLT_DISP_COLS  40  /* Default number of columns the display has */
#define DFLT_NUM_CONTROLLERS 8 /* default numebr of controlers _per_port_ */
#define MAX_DISP_ROWS 4 /* maximal values for hd44780 */
#define MAX_DISP_COLS 40
#define LCD_MAJOR 120 /* Major for device file */
#define TABSTOP   3 /* Length of tabs */
#define MAX_PORTS 1
#define LOAD_CGRAM
#define PHC_TTY_MAJOR 244
#define PHC_TTY_MINORS 1
#define PHC_KB_MAJOR 200
#include "cgram/default.h"
#include "cgram/charmap.h"
#define DEBUG 1
/* #define DEBUG_HD44780 1 */
/* #define DEBUG_IO - PDEBUG_IO */

/**
 * These defines are for the only availble pins on a parallel port
 * which we can use for output besides pins 2 throuh 9 which are used
 * for data. The address for the below pins is IO + 2 eg. 0x378 + 0x02.
 * Some pins on the parallel port are logicaly reversed which is why
 * some which are ON are 0x00. also see parport.h for more info
 * pins: 1 14 16 17 - output pins
 * pins: 10 11 12 13 15 - input pins
 * pin 10 is irq
 */
#include <linux/parport.h>
#define PIN_1_ON	0x00
#define PIN_1_OFF	PARPORT_CONTROL_STROBE
#define PIN_14_ON	0x00
#define PIN_14_OFF 	PARPORT_CONTROL_AUTOFD
#define PIN_16_ON	PARPORT_CONTROL_INIT
#define PIN_16_OFF	0x00
#define PIN_17_ON	0x00
#define PIN_17_OFF	PARPORT_CONTROL_SELECT

#define RS_DATA     PIN_1_ON
#define RS_INST     PIN_1_OFF
#define RW_READ     PIN_14_ON
#define RW_WRITE    PIN_14_OFF
#define CNT_OFF     (PIN_16_OFF | PIN_17_OFF)
#define CNT_NEXT    (PIN_16_OFF	| PIN_17_ON)
#define CNT_ENABLE  (PIN_16_ON	| PIN_17_OFF)
#define CNT_RESET   (PIN_16_ON 	| PIN_17_ON)
#define CNT_LENGTH	10

#endif
