#define r_str(x)        (parport_pc_read_status(lcddev[(x)].pd->port))

static void irq_worker(struct work_struct *mywork)
{
	struct lcdmod_dev *lcddev = container_of(mywork, struct lcdmod_dev, work.work);
	if (lcddev->irq_buff != parport_read_status( lcddev->pd->port )) return;
	PDEBUG(" irq_worker: %#4x %#4x %#4x \n", lcddev->irq_buff, lcddev->status, parport_read_status( lcddev->pd->port ));
	lcddev->status = lcddev->irq_buff;
	PDEBUG("Irq occured \n");
	set_bit(FLAG_WAS_IRQ, &lcddev->flags);
	if (waitqueue_active(&irq_wait)) wake_up_all(&irq_wait);
	if (asyncqueue) kill_fasync(&asyncqueue, SIGIO, POLL_MSG);
}

static void lcdmod_irq(void *handle)
{
	struct lcdmod_dev *lcddev = handle;
	if ( schedule_delayed_work(&lcddev->work, (1*HZ) / 10) ) return;
	PDEBUG(" lcdmod_irq: %#4x %#4x %#4x \n", lcddev->irq_buff, lcddev->status, parport_read_status( lcddev->pd->port ));
	lcddev->irq_buff = parport_read_status( lcddev->pd->port );
}

