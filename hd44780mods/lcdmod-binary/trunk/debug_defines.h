/* after many different things i decided to put it all here :D */
//#define DEBUG
//#define DEBUG_TIMER
//#define DEBUG_OPERATION
//#define DEBUG_HD44780 

#ifdef DEBUG
#	define PDEBUG(fmt, ...)  pr_info("lcdmod: " fmt, ##__VA_ARGS__)
#else
#	define PDEBUG(fmt, ...) /* pr_debug("lcdmod: " fmt, ##__VA_ARGS__) */
#endif

#ifdef DEBUG_TIMER
#	define	PDEBUG_TIMER(fmt, ...) printk("lcdmod: timer: " fmt "\n", ##__VA_ARGS__)
#else
#	define	PDEBUG_TIMER(fmt, ...)
#endif


#ifdef DEBUG_OPERATION
#	define	PDEBUG_OPERATION(fmt, ...) printk("lcdmod: operation: " fmt, ##__VA_ARGS__)
#else
#	define	PDEBUG_OPERATION(fmt, ...)
#endif

#ifdef DEBUG_HD44780
#	define	PDEBUG_hd44780(fmt, ...) printk("lcdmod: hd44780: " fmt, ##__VA_ARGS__)
#else
#	define	PDEBUG_hd44780(fmt, ...)
#endif
