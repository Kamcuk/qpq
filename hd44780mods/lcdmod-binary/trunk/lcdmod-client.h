#ifndef _LCDMOD_CLIENT_H
#define _LCDMOD_CLIENT_H

#include <linux/ioctl.h>

#define LCDMOD_IOC_MAGIC  0xFA /* not in ioctl-number.txt ;) */

struct lcdmod_cgram_t {
	int ctrl_nr;
	int cgram_index;
	unsigned char cgram[8];
};

/* ioctl definitions */
#define LCDMOD_CGRAM_ONELCD	_IOW(LCDMOD_IOC_MAGIC, 1, struct lcdmod_cgram_t)
#define LCDMOD_CGRAM_ALLLCD	_IOW(LCDMOD_IOC_MAGIC, 2, struct lcdmod_cgram_t)
#define LCDMOD_SETLINEWRAP	_IOW(LCDMOD_IOC_MAGIC, 3, int)
#define LCDMOD_GETLINEWRAP	_IOR(LCDMOD_IOC_MAGIC, 4, int)
#define LCDMOD_BYPASS_DATA	_IOW(LCDMOD_IOC_MAGIC, 5, char)
#define LCDMOD_BYPASS_INST	_IOW(LCDMOD_IOC_MAGIC, 6, char)
#define LCDMOD_GETSTATUS       	_IOR(LCDMOD_IOC_MAGIC, 7, int)
#define LCDMOD_GETPORTSNUM     	_IOR(LCDMOD_IOC_MAGIC, 8, int)
#define LCDMOD_WAS_IRQ		_IOR(LCDMOD_IOC_MAGIC, 9, int)
#define LCDMOD_CHARMAP_ENABLED	_IOR(LCDMOD_IOC_MAGIC, 10, int)

/* more to come, remember changing LCMOD_IOC_MAXNR in lcdmod-lib.h */


#endif /* _LCDMOD_CLIENT_H */
