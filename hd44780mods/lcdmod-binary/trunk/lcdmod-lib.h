#ifndef _LCDMOD_LIB_H
#define _LCDMOD_LIB_H

#include "lcdmod-client.h"

#define LCDMOD_IOC_MAXNR  11 /* max number for ioctl(last + 1) */

#define PINFO(fmt, ...) pr_info("lcdmod: " fmt, ##__VA_ARGS__)
#define PWARN(fmt, ...) pr_warn("lcdmod: warning: " fmt, ##__VA_ARGS__)
#define PERR(fmt, ...) pr_err("lcdmod: err:" fmt, ##__VA_ARGS__)

/* lcdmod_dev->flags */
#define FLAG_IRQ_OFF		0
#define FLAG_WAS_IRQ 		1
#define FLAG_WORKER_PENDING 	2
#define FLAG_NOBFCHECK		3
#define FLAG_CHARMAP_DISABLED	4
#define FLAG_WRAP_OFF		5
#define FLAG_FLAGS_SET		6
#define FLAG_READ_OFF		7

struct lcdmod_lcd { /* struct for every lcd */
	unsigned char cgram[8][8]; /* cgram stored */
	unsigned int cur_row, cur_col; /* cursor position */
	unsigned int errors_num;
};

struct lcdmod_dev { /* struct for every port */
	struct pardevice *pd;
	struct lcdmod_lcd lcd[NUM_CONTROLLERS]; /* info about every single lcd */
	
	struct delayed_work work;
	unsigned long flags; /* flags per port */
	short int irq_buff;
	short int status;
	
	/* counters */
	unsigned long int reset_num;
	unsigned long int next_num;
	unsigned long int write_num;
	unsigned long int read_num;
	unsigned long int bfchecks_num;
};


#endif /* ifdef _LCDMOD_LIB_H_ */
