#ifndef _LCDMOD_CONFIG_
#define _LCDMOD_CONFIG_

#define DFLT_BASE 0x378 /* Default I/O base address of the parallel port */
#define DISP_COLS 40  /* number of columns the display has */
#define DISP_ROWS 16  /* number of rows */
#define ROWS_PER_CTRL 2 /* number of rows the display has */
#define NUM_CONTROLLERS 8 /* ( DISP_ROWS / DISP_ROWS_PER_CTRL ) */  /* number of controlers _per_port_ */
#define TABSTOP   3 /* Length of tabs */
#define MAX_PORTS 1
#define DEV_FILE_NAME "lcd" /* file created in /dev */

/* load default cgram? */
#define LOAD_CGRAM
#include "cgram/default.h" 

/** * settings delays global
 * MC14555 operate time - 440 ns
 * HCF4017BE operate time -
 * 	next pulse width - npw = 200ns
 * 	propagation next delay time - pndt = 650 ns
 * 	reset pulse width - rpw = 260 ns
 * 	propagations reset delay time - prst = 530 ns
 * HCF4081BE operate time - 250 ns
 * hd44780 operate time - 
 * 	address set up time - 40 ns
 * 	pulse width - 230 ns
 * 	minimum circle time - 500 ns
 * if you error, try manipulating with these values.
 ** * settings * delays when no busyflag checkig * LONG_DELAY & NORMAL_DELAY
 * Minimum time to wait after most commands is 39us (for safe i sleep 50us)
 * Clear Display and Return Home need to wait 1.53ms(for safe i sleep 16ms)
 * LOW_VOLTAGE displays operate much slower: normal delay ~160u and long delay ~20ms
 * those values will be used if you comment the busyflag checking section in write and read functions
 * this sleep wil use ppl who cannot read from devices.
 * remember that if you do not check the busyflag,
 *   then sleeps _should_ last longer then in the table in the documentation (so says documentation! xD )
 ** * settings * delays associated with busyflag checking 
 * sometimes when the cable isn't working, busyflag is not checked and it stucks into
 * an infinity loop (wait for busyflag to be low, but couse od no connection
 * it stays high), thats why i added BUSYFLAG_STUCK, it quits checking BF after 
 * so many times it checked, and unsets BF checking 
 */
#define ADDRESS_SET_UP_TIME() 	ndelay(140)  /* 40 ns for hd44780 + 100*/
#define ENABLE_PULSE_WIDTH() 	ndelay(720) /* 4555 + 4081 + 230 ns for hd44780 - 100 */
#define DATA_HOLD_TIME() 	ndelay(780) /* 4555 + 4081 + 10ns for hd44780 + 100 */
#define READ_EXTRA_DELAY()	udelay(1) /* i figured out, that reading needs extra delay */
#define NEXT_PULSE_WIDTH() 	ndelay(840) /* 4555 + npw 4017 + 200 */
#define NEXT_SET_UP_TIME() 	ndelay(1190) /* 4555 + npw 4017 + pndt + 200 */
#define RESET_PULSE_WIDTH() 	ndelay(1200) /* 4555 + rsw 4017 + 200 */
#define RESET_SET_UP_TIME() 	ndelay(1270)  /* 4555 + prst 4017 + 200 */
/* delays when NO busyflag checking - LONG_DELAY and NORMAL_DELAY */
#define LONG_DELAY() do {set_current_state(TASK_INTERRUPTIBLE); schedule_timeout( (int)( (16 * HZ) / 10000 ) );} while(0)
//#define LONG_DELAY() udelay(1600)
#define NORMAL_DELAY() udelay(50)
/* delays when busyflag checking - LONG_DELAY and NORMAL_DELAY */
#define BUSYFLAG_DELAY() 	ndelay(10) /* the delay between BF checks */
#define BUSYFLAG_STUCK 		1000 /* how many times check busyflag to get stuck? */


/* you want some DEBUG info eh? look here */
#include "debug_defines.h"
 


/**
 * These defines are for the only availble pins on a parallel port
 * which we can use for output besides pins 2 throuh 9 which are used
 * for data. The address for the below pins is IO + 2 eg. 0x378 + 0x02.
 * Some pins on the parallel port are logicaly reversed which is why
 * some which are ON are 0x00. also see parport.h for more info
 * pins: 1 14 16 17 - output pins
 * pins: 10 11 12 13 15 - input pins
 * pin 10 is irq
 */
#include <linux/parport.h>
#define PIN_1_ON	0x00
#define PIN_1_OFF	PARPORT_CONTROL_STROBE
#define PIN_14_ON	0x00
#define PIN_14_OFF 	PARPORT_CONTROL_AUTOFD
#define PIN_16_ON	PARPORT_CONTROL_INIT
#define PIN_16_OFF	0x00
#define PIN_17_ON	0x00
#define PIN_17_OFF	PARPORT_CONTROL_SELECT

#define RS_DATA     PIN_1_ON
#define RS_INST     PIN_1_OFF
#define RW_READ     PIN_14_ON
#define RW_WRITE    PIN_14_OFF
#define CNT_OFF     (PIN_16_OFF | PIN_17_OFF)
#define CNT_NEXT    (PIN_16_OFF	| PIN_17_ON)
#define CNT_ENABLE  (PIN_16_ON	| PIN_17_OFF)
#define CNT_RESET   (PIN_16_ON 	| PIN_17_ON)
#define CNT_LENGTH	10

#endif
