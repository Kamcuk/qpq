#ifndef _hd44780_operations_h_
#define _hd44780_operations_h_

/** global gived functions */
unsigned char hd44780_read(unsigned int ctrl_nr, int flags);

void hd44780_write(unsigned int ctrl_nr, int flags, unsigned char command);

void hd44780_write_all(int flags, unsigned char command);

/** init the port counter and maybe other stuff */
void port_init(void);

/* global usefull macros */
#define write_inst(ctrl, inst)		hd44780_write(ctrl, RS_INST, inst)
#define write_data(ctrl, data)		hd44780_write(ctrl, RS_DATA, data)
#define read_inst(ctrl)			hd44780_read(ctrl, RS_INST)
#define read_data(ctrl)			hd44780_read(ctrl, RS_DATA)
#define write_inst_all(inst)		hd44780_write_all(RS_INST, inst)
#define write_data_all(data)		hd44780_write_all(RS_DATA, data)

#endif /* _hd44780_operations_h_ */
